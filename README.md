# 0202 - Projection on a Half-sphere

## Short Description

<table align="center">
    <tr>
    <td align="left" style="font-style:italic; font-size:12px; background-color:white">Explore the Earth and Moon!<br>
        Use the touchscreen of the control station to look at different visualizations of the Earth.<br>
        Get more information at the control station.</td>
    </tr>
</table>

The application shows a selection of Earth and Moon visualizations projected on a sphere. 
The user can select the images / videos shown on a sphere by operating the control station. 

In the exhibition, the application is projected on a hemisphere hanging on a wall. From the control station various visualizations can be chosen: a selection of NASA’s GEOS-5 Nature Run videos, or different views of the Earth / Moon, as well as their grids. The control station offers more information about individual visualizations. By waving his / her hand above the Leap Motion controller the user can rotate the Earth / Moon. Two buttons in the lower left corner of the control station allow the user to restart the application or change the language (English or German). 

This application is used at the [ESO Supernova Planetarium and Visitor Centre](https://supernova.eso.org/?lang=en), Garching b. München.  
For more details about the project and how applications are run and managed within the exhibition please see [this link](https://gitlab.com/HITS_Supernova/overview).   


## Requirements / How-To

This station uses a computer with both a projector and a touch screen attached (both have 1280x800 pixels resolution). The projector displays the image on a half-sphere while the touch screen displays the available image or video options, allows the user to change the displayed content and gives additional explanations. The start-up script `2in1.sh` arranges the windows on the displays, sets the resolution and starts the different software components of the station. Leap Motion sensor is connected via USB port and allows the user to rotate the sphere. 

**WebGL**  
A browser with a WebGL support is needed to run the interactive (start `WebGL/webgl_SphereProjection.html`).  
Keys 1-9 can be used to see various visualizations of the Earth / Moon without using the Python part of the application.  

**Touch Controller**  
The [Touch Controller](https://gitlab.com/HITS_Supernova/0000_touchcontroller) application is used to control the image or video shown on the projection with an additional touch screen. Communication with projection application (WebGL) happens through websockets. It is started by the wrapper script `touch-controller/start.sh`). Available textures are defined in `touch-controller/content_earth_moon.yaml`.

## Detailed Information

#### Webpage Keyboard keys (for calibration and testing):   

Web browser part of the interactive can be tested using the keyboard.  
Rendered object (Earth or Moon) needs to calibrated in order to correctly fit the hemisphere it is projected on. 
Calibration can be done via keyboard. Earth/Moon object can be moved/scaled and its parts hidden using an ellipse mask surrounding the object. 

Keyboard keys:

    - test images: "1" - "9"   
    - test overlays: "+"/"-" grid on/off  
    - reload: "r"
    - show/hide calibration data: "A"/"W" 
    - ellipse mask:
        - color black, gray, white: "T", "Y", "U" 
        - ellipse center left, right, down, up: "X", "C", "S", "D"
        - ellipse height less, more: "H", "J" (snaps to center (x,0)) 
        - ellipse width less, more: "N", "M" (snaps to center (0,y)) 
        - ellipse radius less, more: "F", "G"
        - ellipse border blur less (sharper), more (blurrier): "V", "B"
 
Rendered sphere (Earth or Moon) position, size and camera field of view can be changed using the calibration GUI ("Apply" button needs to be pressed for the changes to take effect). 
Calibration parameters are saved into the URL parameters by pressing "Save URL" button.  
See the [image](WebGL/Images/0202_calibration.png).  
         

#### URL parameters (webpage)

Calibration URL parameters (will be set upon save):  

    b - mask border blur (INborderShader)  
    r - mask radius (INradiusShader)  
    w - mask width (INwidthParamShader)  
    h - mask height (INheigthParamShader)  
    cx - mask center X position (INcenterParamX)  
    cy - mask center Y position (INcenterParamY)  
    s - object (sphere) size (INssize)  
    f - camera field of view (INfov)  
    ex - object (sphere) position X (INearthPosX)  
    ey - object (sphere) position Y (INearthPosY)  

#### Client/server communication 

WebSockets are used to communicate with the python GUI on a small touchscreen. 

    Websockets port: 8889  
    Websockets example message: {"texture":imageSource, "overlay_grid":imageSource}  
    Websockets example reset message {"texture":"Reset"}

#### (Optional) Using the Additional Scripts

Applications within the [Hilbert](https://github.com/hilbert) Museum Management System are used as [Docker](https://www.docker.com/) containers.   
Dockerized version of the application uses the `Dockerfile`, `Makefile` and various bash scripts to properly start the station.     

Application uses the following scripts to start: 

* `docker-compose.hitshalfsphere.yml` (yml file not available):
    * starts `2in1.sh`:  
        * starts Leap Motion daemon
        * starts python GUI (`touch-controller/start.sh`)
        * shows the WebGL application on the projector screen, and Python GUI on a separate touchscreen
        * starts WebGL application using Hilbert kiosk browser (`WebGL/webgl_SphereProjection.html`)
        
* `Dockerfile` additionally does the following:
    * installs the Leap Motion package in the docker image (`Leap-2.3.1+31549-x64.deb`) (not in this repository)


## Credits

This application was developed by the ESO Supernova team at [HITS gGmbH](https://www.h-its.org/en/).  
Idea by Kai Polsterer, Dorotea Dudas and Volker Gaibler, HITS gGmbH.  
WebGL sphere projection application by Dorotea Dudas.  
Python controller application by Volker Gaibler.  

#### Code Licensing 

* This code is licensed as: [MIT license](LICENSE)
* MIT license:
    * *Three.js* by Mr.doob (Ricardo Cabello) [source](https://threejs.org/)
    * *jQuery* [source](https://jquery.com/)
    * *THREEx.KeyboardState.js* by Jerome Etienne [threex.keyboardstate](https://github.com/jeromeetienne/threex.keyboardstate) 
    * postprocessing Mask Shader *EllipseMaskShader.js* by Dorotea Dudas
* *LeapJS v0.6.4* by LeapMotion, Inc. and other contributors [leapjs@GitHub](http://github.com/leapmotion/leapjs/) [(Apache-2.0 license)](https://github.com/leapmotion/leapjs/blob/master/LICENSE)

#### Image, Animation Licensing 

* CC BY 4.0:
    * Earth clouds image, Moon image provided by ESO (Earth map too dark)
    * Overlay images: Earth, Moon grid images from ESO / HITS gGmbH
* [NASA Visible Earth](https://visibleearth.nasa.gov/view_cat.php?categoryID=1484) [(Media Usage Guidelines)](https://www.nasa.gov/multimedia/guidelines/index.html)
    * Earth Images: Earth map [image](https://visibleearth.nasa.gov/view.php?id=73909) 
    * Earth's City Lights [image](https://visibleearth.nasa.gov/view.php?id=55167) 
* Earth Animations from [GEOS-5 Nature Run Collection](https://svs.gsfc.nasa.gov/30017):
    * Aerosol simulation: Dust (red), sea salt (blue), organic/black carbon (green), and sulfates (white) displayed by their extinction aerosol optical thickness.This simulation used GEOS-5 and the Goddard Chemistry Aerosol Radiation and Transport (GOCART) Model.
    * Precipitation simulation: Total precipitable water (white) and rainfall (colors 0-15 millimeters/hour; red=highest).
    * Wind simulation: Surface winds (white 0-40 meters/second) and upper-level (250 hPa) winds (colors 0-175 meters/second; red=faster).
    * Surface Temperature simulation: Surface temperature (colors 270-310 Kelvin) and outgoing longwave radiation at the top of the atmosphere (white) representative of clouds in the model.
    * Earth’s CO2 simulation [A Year In The Life Of Earth’s CO2](https://svs.gsfc.nasa.gov/11719)
* *Elevation.jpg* Earth elevation map [source](https://de.wikipedia.org/wiki/Datei:Elevation.jpg) (Public Domain)

        
        


