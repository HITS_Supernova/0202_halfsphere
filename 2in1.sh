#!/bin/bash

# start leap motion daemon
echo "[+] Checking connected USB devices with lsusb:"
lsusb

# define env variable with vendor:product id for Leap Motion to make add a strict check of presence,
# e.g. "f182:0003"
if [[ $HILBERT_LEAPMOTION_ID ]] ; then
    if lsusb -d "$HILBERT_LEAPMOTION_ID" >/dev/null ; then
        echo "[+] OK: Leap Motion Controller found."
    else
        echo "[+] ============================================================"
        echo "    ERROR: Leap Motion Controller $HILBERT_LEAPMOTION_ID not found!"
        echo "    Please check hardware connection and 'lsusb' on host system."
        echo "    ============================================================"
    fi
fi

echo "[+] Starting leap motion daemon 'leapd' ..."
/usr/sbin/leapd &
pid_leap="$!"
sleep 3

echo "[+] Setting up screens ..."
#    9  pkill kiosk-browser
xrandr -q
xinput --list

TOUCH_DEVICE="ILITEK ILITEK Multi-Touch"
xinput --list "${TOUCH_DEVICE}"

# 10" touchscreen
GUI="DP-5"
# beamer
APP="HDMI-0"

GUI_X="0"
GUI_Y="0"
GUI_W="1280"
GUI_H="800"

xinput map-to-output "${TOUCH_DEVICE}" "${GUI}"

APP_X="${GUI_W}"
APP_Y="0"

APP_W="1280"
APP_H="800"

xrandr --output "${APP}" --mode "${APP_W}x${APP_H}" --pos "${APP_X}x${APP_Y}" --rotate normal

xrandr --output "${GUI}" --mode "${GUI_W}x${GUI_H}" --pos "${GUI_X}x${GUI_Y}" --rotate normal

xrandr -q

cd /I

touch-controller/start.sh &

pid_tc="$!"
echo "=> GUI PID: $pid_tc"
sleep 1
xwininfo -tree -root


## pid x y w h
function window_set() {
  echo "Running 'window_set' with the following arguments: [$@]!"
  PID="$1"
  shift
  sleep 1
  WID=`xdotool search --sync --onlyvisible --pid $PID 2>/dev/null`
  echo "PID: $PID => WID: $WID"
  xprop -id ${WID}
  echo "Moving the window to [$1 x $2]"
  xdotool windowmove ${WID} $1 $2
  shift
  shift
  sleep 1
  echo "Resizing the window to [$1 x $2]"
  xdotool windowsize -sync ${WID} $1 $2
}

window_set "$pid_tc" "${GUI_X}" "${GUI_Y}" "${GUI_W}" "${GUI_H}"

## name x y w h
function window_set_name() {
  echo "Running 'window_set_name' with the following arguments: [$@]!"
  PID="$1"
  shift
  sleep 1
  xwininfo -tree -root
  WID=`xdotool search --limit 1 --sync --onlyvisible --name "$PID" 2>/dev/null`
  echo "PID: $PID => WID: $WID"
  xprop -id ${WID}
  echo "Moving the window to [$1 x $2]"
  xdotool windowmove ${WID} $1 $2
  shift
  shift
  sleep 1
  echo "Resizing the window to [$1 x $2]"
  xdotool windowsize -sync ${WID} $1 $2
  xrandr -q
}


#GUIid=`xdotool search --sync --onlyvisible --pid $pid_tc 2>/dev/null`
#xdotool windowmove ${GUIid} ${GUI_X} ${GUI_Y}
#sleep 1
#xdotool windowsize -sync ${GUIid} ${GUI_W} ${GUI_H}
#1280 800
#calibration		#b=0.08&r=0.275&w=1644&h=1500&cx=0.355&cy=0.255&s=248&f=30&ex=-28&ey=30

# calibration done for ESO Supernova. 2019-02-04.
#
# original setting:
#browsercmd="hb_preload.sh browser.sh -z 1.0 -l file:///I/WebGL/webgl_SphereProjection.html?HB_APP_ID=${APP_ID}\&HB_URL=${HB_URL}\&b=0.08\&r=0.275\&w=1644\&h=1500\&cx=0.355\&cy=0.255\&s=248\&f=30\&ex=-28\&ey=31"

# hide polar "holes":
browsercmd="hb_preload.sh browser.sh -z 1.0 -l file:///I/WebGL/webgl_SphereProjection.html?HB_APP_ID=${APP_ID}\&HB_URL=${HB_URL}\&b=0.08\&r=0.270\&w=1740\&h=1496\&cx=0.343\&cy=0.2674\&s=131\&f=14\&ex=-14\&ey=16"

# max visible size, but with polar "holes":
#browsercmd="hb_preload.sh browser.sh -z 1.0 -l file:///I/WebGL/webgl_SphereProjection.html?HB_APP_ID=${APP_ID}\&HB_URL=${HB_URL}\&b=0.01\&r=0.327\&w=1500\&h=1496\&cx=0.392\&cy=0.304\&s=131\&f=14\&ex=-14\&ey=16"

echo "Starting browser with command: $browsercmd"
/bin/bash -c "$browsercmd" &

pid_browser="$!"
echo "=> APP PID: $pid_browser"
sleep 1
xwininfo -tree -root
#sleep 1
# as above

#APPid=`xdotool search --sync --onlyvisible --pid $pid_browser 2>/dev/null`
# 0x1000001
#xdotool windowmove ${APPid} ${APP_X} ${APP_Y}
#sleep 1
#xdotool windowsize -sync ${APPid} ${APP_W} 10
#sleep 1
#xdotool windowsize -sync ${APPid} ${APP_W} ${APP_H}

N="Earth"
window_set_name "$N" "${APP_X}" "${APP_Y}" "${APP_W}" "${APP_H}"


xinput map-to-output "${TOUCH_DEVICE}" "${GUI}"

xwininfo -tree -root

# wait until one of these dies
echo "[+] PIDs:"
echo "    pid_browser=$pid_browser pid_tc=$pid_tc pid_leap=$pid_leap this_script=$$"
echo "[+] 2in1.sh: Waiting until one of these child processes dies or container is stopped ..."
wait -n $pid_browser $pid_tc $pid_leap

# keep info about processes for debugging
echo "[+] The 'wait' command returned! Status of respective processes:"
ps u --pid $$,$pid_browser,$pid_tc,$pid_leap

echo "Done."
exit 0
