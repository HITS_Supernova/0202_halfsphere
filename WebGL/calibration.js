//calibration setup
//to add parameters for changing: add values into LoadDefalutParameters() and reparametrize() (after adding logic and input forms)
//to add to url parameters: add to "overide default", add to save saveParameters()
	
{//set some default input values (width, height)
	//set the width/height input value
	document.getElementById("INwidthParamShader").value = window.innerWidth;
	document.getElementById("INheigthParamShader").value = window.innerHeight;
}
	
{//override default input values if url parameters are there (getUrlVars())
	function getUrlVars() {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
			function(m,key,value) {
				vars[key] = value;
			}
		);
		return vars;
	}
	if(getUrlVars()["b"]) document.getElementById("INborderShader").value = parseFloat(getUrlVars()["b"]);
	if(getUrlVars()["r"]) document.getElementById("INradiusShader").value = parseFloat(getUrlVars()["r"]);
	if(getUrlVars()["w"]) document.getElementById("INwidthParamShader").value = parseInt(getUrlVars()["w"]);
	if(getUrlVars()["h"]) document.getElementById("INheigthParamShader").value = parseInt(getUrlVars()["h"]);
	if(getUrlVars()["cx"]) document.getElementById("INcenterParamX").value = parseFloat(getUrlVars()["cx"]);
	if(getUrlVars()["cy"]) document.getElementById("INcenterParamY").value = parseFloat(getUrlVars()["cy"]);
	if(getUrlVars()["s"]) document.getElementById("INssize").value = parseFloat(getUrlVars()["s"]);
	if(getUrlVars()["f"]) document.getElementById("INfov").value = parseFloat(getUrlVars()["f"]);
	if(getUrlVars()["ex"]) document.getElementById("INearthPosX").value = parseFloat(getUrlVars()["ex"]);
	if(getUrlVars()["ey"]) document.getElementById("INearthPosY").value = parseFloat(getUrlVars()["ey"]);
}
			
{//preload values (taken from input from values - which are either the hardcoded ones or the ones from URL parameters) (LoadDefalutParameters())
	var borderShader, radiusShader, widthParamShader, heigthParamShader, centerParamX, centerParamY;
	var colParam, ssize, fov;
	var earthPositionX, earthPositionY;
	function LoadDefalutParameters(){
		borderShader = document.getElementById("INborderShader").value;
		radiusShader = document.getElementById("INradiusShader").value;
		widthParamShader = document.getElementById("INwidthParamShader").value;
		heigthParamShader = document.getElementById("INheigthParamShader").value;
		centerParamX = parseFloat(document.getElementById("INcenterParamX").value);
		centerParamY = parseFloat(document.getElementById("INcenterParamY").value);
//       	var colParam = document.getElementById("INcolParam").value;			  
		colParam = 1.0;
// // 		var camParam = document.getElementById("INcamParam").value;
		ssize = parseInt(document.getElementById("INssize").value);
// // 		var cam_dist = document.getElementById("INcam_dist").value;
		fov = parseInt(document.getElementById("INfov").value);
		earthPositionX = parseFloat(document.getElementById("INearthPosX").value);
		earthPositionY = parseFloat(document.getElementById("INearthPosY").value);
	}
}
			
{//button functions (Apply, B,G,W, Reset, Clear, Save) (reparametrize() - apply button, saveParameters() - set url, setDefaultParameters(), reset() - clear url)
	function reparametrize(){//Apply
		console.log("apply");
		{//border blur up/down
		borderShader = document.getElementById("INborderShader").value;
		effect.uniforms[ 'border' ].value = borderShader;
		}
		{//radius up/down
		radiusShader = document.getElementById("INradiusShader").value;
		effect.uniforms[ 'radius' ].value = radiusShader;
		}
		{//width up/down
		widthParamShader = document.getElementById("INwidthParamShader").value;
		effect.uniforms[ 'xRes' ].value = widthParamShader;
		}
		{//height up/down
		heigthParamShader = document.getElementById("INheigthParamShader").value;
		effect.uniforms[ 'yRes' ].value = heigthParamShader;
		}
		{//center x left/right
		centerParamX = parseFloat(document.getElementById("INcenterParamX").value);
		effect.uniforms[ 'xCenter' ].value = centerParamX;
		//if coupled then = (window.innerWidth/widthParamShader)/2. ;
		}
		{//center y left/right  
		centerParamY = parseFloat(document.getElementById("INcenterParamY").value);
		effect.uniforms[ 'yCenter' ].value = centerParamY; 
		//if coupled then = (window.innerHeight/heigthParamShader)/2. ;
		}
		{//mask color (1 black, 2 gray, 3 white) - moved to separate functions below (maskToWhite(), maskToGray(), maskToBlack())
		}
// // 		camParam = document.getElementById("INcamParam").value;
		//camera distance
// 		cam_dist = document.getElementById("INcam_dist").value;				
		{//Earth's position X, Y
		earthPositionX = parseFloat(document.getElementById("INearthPosX").value);
		earthPositionY = parseFloat(document.getElementById("INearthPosY").value);
		}
		{//sphere size
		ssize = parseInt(document.getElementById("INssize").value);
		RemoveObjects();
		InitObjects();
		}
		{//field of view
		fov = parseInt(document.getElementById("INfov").value);
		camera.fov = fov;
		camera.updateProjectionMatrix();
		}
	}
	function maskToWhite(){//mask color (1 black, 2 gray, 3 white)
// 		colParam = document.getElementById("INcolParam").value;
		effect.uniforms[ 'colorParam' ].value = 3.0;//white  
	}
	function maskToGray(){//mask color (1 black, 2 gray, 3 white)
// 		colParam = document.getElementById("INcolParam").value;
		effect.uniforms[ 'colorParam' ].value = 2.0;//gray			  
	}
	function maskToBlack(){//mask color (1 black, 2 gray, 3 white)
// 		colParam = document.getElementById("INcolParam").value;
		effect.uniforms[ 'colorParam' ].value = 1.0;//black			  
	}			

	function saveParameters() {//set url parameters
		console.log("save");
		window.location.href = window.location.pathname+"?b="+borderShader + "&r=" + radiusShader + "&w=" + widthParamShader + "&h=" + heigthParamShader 
		+ "&cx=" + centerParamX + "&cy=" + centerParamY + "&s=" + ssize + "&f=" + fov + "&ex=" + earthPositionX + "&ey=" + earthPositionY;
	}			
	function setDefaultParameters() {//just clear the url parameters and load defaults
		console.log("default");
		window.location.href = window.location.pathname;//+"?lang="+lang;
	}				
	function reset() {//just clear the url parameters and load defaults
		console.log("reset");
		window.location.reload(true);
	}
}
