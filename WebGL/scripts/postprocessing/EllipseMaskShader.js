/**
 * @author Dorotea Dudas 2015
 * dorotea.dudas@h-its.org
 * MIT licence
 * 
 */

THREE.EllipseMaskShader = {
	uniforms: {
		"tDiffuse": { type: "t", value: null },//screen
		"xRes":		{ type: "f", value: 1.0 }, 
		"yRes":		{ type: "f", value: 1.0 }, 
		"xCenter":	{ type: "f", value: 1.0 }, 
		"yCenter":	{ type: "f", value: 1.0 }, 		
		"radius":	{ type: "f", value: 1.0 }, 
		"border":	{ type: "f", value: 1.0 }, 
		"colorParam":	{ type: "f", value: 1.0 },		
	},

	vertexShader: [
		"varying vec2 vUv;",
		"void main() {",
			"vUv = uv;",
			"gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
		"}"
	].join("\n"),

	fragmentShader: [
		"uniform sampler2D tDiffuse;",
		"varying vec2 vUv;",
		"uniform float xRes;",
		"uniform float yRes;",		
		"uniform float xCenter;",
		"uniform float yCenter;",		
		"uniform float radius;",
		"uniform float border;",
		"uniform float colorParam;",		

		"void main() {",
			"vec2 uv = gl_FragCoord.xy / vec2(xRes,yRes) ;",//"vec2 uv = vUv ;",//"vec2 uv = -1.0 + 2.0 * vUv;",
			
			"vec2 m = uv - vec2(xCenter, yCenter);",//0.5,0.5
			"float dist = radius - sqrt(m.x * m.x + m.y * m.y);",//circle of a radius

			//blend/mix parameter t
			"float t = 0.0;",
			"if (dist > border) t = 1.0;",//clean border//"if (dist > 0.1) t = 1.0;",//"if (dist > 0.0) t = 1.0;",
			"else if (dist > 0.0) t = dist / border;",//blurry border
			
			//mask color
			"vec4 black = vec4(0.0, 0.0, 0.0, 0.1);",
			"vec4 white = vec4(1.0, 1.0, 1.0, 0.1);",
			"vec4 gray = vec4(0.5, 0.5, 0.5, 0.1);",
			"vec4 blue = vec4(0.0, 0.0, 1.0, 0.1);",
			
			"vec4 ccolor = blue;",
			"if(colorParam == 1.0) ccolor = black;",
			"if(colorParam == 2.0) ccolor = gray;",
			"if(colorParam == 3.0) ccolor = white;",

			//screen texture
			"vec4 screen = texture2D( tDiffuse, vUv );",
			
			//result
			"gl_FragColor = mix(ccolor, screen, t);",//"gl_FragColor = mix(ccolor, black, t);",
			"gl_FragColor.a = 0.1;",
		"}"
	].join("\n")
};
