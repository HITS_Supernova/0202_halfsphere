// 		<!--websockets-->
// 		<div id="result" style="visibility: visible"><!--hidden-->
// 			  <div id="status">Connecting...</div>
// 			  <button type="button" id="close">Close Connection</button>
// 			  <ul id="messagesSent"></ul>
// 			  <ul id="messagesReceived"></ul>
// 		</div>		  			
			
			//websockets
			var messagesListSent = document.getElementById('messagesSent');
			var messagesListReceived = document.getElementById('messagesReceived');
			var socketStatus = document.getElementById('status');
			var closeBtn = document.getElementById('close');
			
// 			// Create a new WebSocket.
			//socket defined in parameters.js
// 			var socket;			
			function WebsocketStart(){
// 				socket = new WebSocket('ws://echo.websocket.org');
				//socket.open();
				
				// Handle any errors that occur.
				socket.onerror = function(error) {
					console.log('WebSocket Error: ' + error);
				};

				// Show a connected message when the WebSocket is opened.
				socket.onopen = function(event) {
					socketStatus.innerHTML = 'Connected to: ' + event.currentTarget.url;
					socketStatus.className = 'open';
					console.log(event.currentTarget);
					console.log(event.currentTarget.url);
				};

				// Handle messages sent by the server.
				socket.onmessage = function(event) {
					message = event.data;
					messagesReceived.innerHTML = '<li class="received"><span>Received:</span>' + message + '</li>';			    
				};

				// Show a disconnected message when the WebSocket is closed.
				socket.onclose = function(event) {
					socketStatus.innerHTML = 'Disconnected from WebSocket.';
					socketStatus.className = 'closed';
				};

				// Close the WebSocket connection when the close button is clicked.
				closeBtn.onclick = function(e) {
					e.preventDefault();
					socket.close();// Close the WebSocket.
					return false;
				};		  						
			}
			WebsocketStart();

			//check connection every 4 seconds and reconnect if not connected
			var ws_checkinterval = 4000;
			function check_websocket() {
				if (!socket || socket.readyState == WebSocket.CLOSED){
					console.log("bla", socket);
					WebsocketStart();
				};
			}
			setInterval(check_websocket, ws_checkinterval);